import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { AdminComponent } from "./admin/admin.component";
import { MainpageComponent } from "./mainpage/mainpage.component";
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { HttpClientModule } from "@angular/common/http";
import { AngularFireModule } from "@angular/fire";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { AngularFirestoreModule } from "@angular/fire/firestore";
import { AngularFireStorageModule } from "@angular/fire/storage";
import { HeaderComponent } from "./header/header.component";
import { DashboardadminComponent } from "./dashboardadmin/dashboardadmin.component";
import { Plyerof2morrowComponent } from "./plyerof2morrow/plyerof2morrow.component";
import { OwlModule } from "ngx-owl-carousel";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import { far } from "@fortawesome/free-regular-svg-icons";
import { fab } from "@fortawesome/free-brands-svg-icons";

library.add(fas, far, fab);

const firebaseConfig = {
  apiKey: "AIzaSyAtia5WvDNs7IR6xvT_OV_sFjXMw8W6PgQ",
  authDomain: "jact-73fc3.firebaseapp.com",
  databaseURL: "https://jact-73fc3.firebaseio.com",
  projectId: "jact-73fc3",
  storageBucket: "",
  messagingSenderId: "631049573719",
  appId: "1:631049573719:web:c9f3c60b60ae900f"
};

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    MainpageComponent,
    HeaderComponent,
    DashboardadminComponent,
    Plyerof2morrowComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    OwlModule,
    NgbModule,
    FontAwesomeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}

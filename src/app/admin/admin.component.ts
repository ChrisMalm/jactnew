import { Component, OnInit, HostBinding } from "@angular/core";
import { NgForm } from "@angular/forms";
import { AuthService } from "../services/auth.service";
import { AngularFireAuth } from "@angular/fire/auth";
import { Router } from "@angular/router";
//import { moveIn } from '../router/router.animations';
import { AngularFirestore } from "@angular/fire/firestore";

@Component({
  selector: "app-admin",
  templateUrl: "./admin.component.html",
  styleUrls: ["./admin.component.css"]
  // animations: [moveIn()],
  // host: {'[@moveIn]': ''}
})
export class AdminComponent implements OnInit {
  // email: string = "malmgrenchristoffer@gmail.com";
  // password: string = "jact2019";

  constructor(public af: AngularFireAuth, public authService: AuthService) {}

  ngOnInit() {}

  // onSubmit(f: NgForm) {
  //   event.preventDefault();
  //   const username = f.value.username;
  //   const password = f.value.password;

  //   this.authService.login(username, password).then(data => {
  //     console.log(data);
  //   });
  //   this.router.navigate(["/main"]);
  // }
}

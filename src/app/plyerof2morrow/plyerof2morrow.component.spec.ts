import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Plyerof2morrowComponent } from './plyerof2morrow.component';

describe('Plyerof2morrowComponent', () => {
  let component: Plyerof2morrowComponent;
  let fixture: ComponentFixture<Plyerof2morrowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Plyerof2morrowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Plyerof2morrowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

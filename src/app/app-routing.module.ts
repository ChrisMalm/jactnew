import { Plyerof2morrowComponent } from "./plyerof2morrow/plyerof2morrow.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MainpageComponent } from "./mainpage/mainpage.component";
import { AdminComponent } from "./admin/admin.component";

const routes: Routes = [
  { path: "", redirectTo: "main", pathMatch: "full" },
  { path: "admin", component: AdminComponent },
  { path: "main", component: MainpageComponent },
  { path: "plyerof2morrow", component: Plyerof2morrowComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}

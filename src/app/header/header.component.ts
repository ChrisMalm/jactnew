import { Component, OnInit } from "@angular/core";
import { AuthService } from "../services/auth.service";
import { faCameraRetro } from "@fortawesome/free-solid-svg-icons";
import { faFacebook, faInstagram } from "@fortawesome/free-brands-svg-icons";
@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"]
})
export class HeaderComponent implements OnInit {
  faCameraRetro = faCameraRetro;
  faFacebook = faFacebook;
  faInstagram = faInstagram;
  constructor(public authService: AuthService) {}

  ngOnInit() {}
}

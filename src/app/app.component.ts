import { Component, OnInit } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import { Router } from "@angular/router";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {
  constructor(private afAuth: AngularFireAuth, public router: Router) {}

  ngOnInit() {
    this.afAuth.authState.subscribe(user => {
      console.log("inloggad användare", user);
    });
  }
}
